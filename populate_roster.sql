ALTER TABLE person ADD tier integer;

INSERT INTO person (extension, contacturi, name, support, accounts, sales) VALUES('200', 'sip:alison@gymmaster.sip.us1.twilio.com', 'Alison', False, True, False);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('201', 'sip:luke@gymmaster.sip.us1.twilio.com', 'Luke', False, False, True, 2);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('202', 'sip:russell@gymmaster.sip.us1.twilio.com', 'Russell', True, False, False, 1);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('207', 'sip:sarah@gymmaster.sip.us1.twilio.com', 'Sarah', True, False, True, 1);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('208', 'sip:ryan@gymmaster.sip.us1.twilio.com', 'Ryan', True, False, False, 2);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('209', 'sip:ben@gymmaster.sip.us1.twilio.com', 'Ben', True, False, True, 2);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('210', 'sip:shamshertony@gymmaster.sip.us1.twilio.com', 'Shamsher/Tony', True, False, False, 1);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('213', 'sip:chris@gymmaster.sip.us1.twilio.com', 'Chris', True, False, False, 2);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('217', 'sip:francis@gymmaster.sip.us1.twilio.com', 'Francis', True, False, False, 2);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('218', 'sip:becky@gymmaster.sip.us1.twilio.com', 'Becky', False, False, True, 2);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('219', 'sip:mal@gymmaster.sip.us1.twilio.com', 'Mal', False, False, True, 2);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('220', 'sip:dylan@gymmaster.sip.us1.twilio.com', 'Dylan', True, False, False, 2);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('222', 'sip:hollie@gymmaster.sip.us1.twilio.com', 'Hollie', False, False, True, 2);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('223', 'sip:michaelr@gymmaster.sip.us1.twilio.com', 'Michael R', True, False, False, 2);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('224', 'sip:jayden@gymmaster.sip.us1.twilio.com', 'Jayden', True, False, False, 2);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('226', 'sip:brendon@gymmaster.sip.us1.twilio.com', 'Brendon', True, False, False, 2);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('227', 'sip:michaelyee@gymmaster.sip.us1.twilio.com', 'Michael Yee', True, False, False, 1);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('250', 'sip:nelly@gymmaster.sip.us1.twilio.com', 'Nelly', True, False, True, 1);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('212', 'sip:callumt@gymmaster.sip.us1.twilio.com', 'Callum T', True, False, False, 2);
INSERT INTO person (extension, contacturi, name, support, accounts, sales, tier) VALUES('999', 'sip:afterhours@gymmaster.sip.us1.twilio.com', 'Afterhours', True, True, True, 1);


-- Afterhours Roster
INSERT INTO roster (starttime, endtime, dow, personid) VALUES('00:00', '05:00', 1, 25);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES('20:00', '24:00', 1, 25);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES('00:00', '01:00', 2, 25);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES('20:00', '24:00', 2, 25);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES('00:00', '01:00', 3, 25);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES('20:00', '24:00', 3, 25);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES('00:00', '01:00', 4, 25);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES('20:00', '24:00', 4, 25);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES('00:00', '01:00', 5, 25);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES('20:00', '24:00', 5, 25);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES('00:00', '01:00', 6, 25);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES('13:00', '24:00', 6, 25);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES('00:00', '24:00', 7, 25);

-- Monday Tier 1
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('05:00', '13:30', 1, 3);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('07:00', '15:00', 1, 17);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('08:30', '14:30', 1, 4);

-- Monday Tier 2
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('09:00', '13:00', 1, 16);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('09:00', '17:00', 1, 8);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('10:30', '18:30', 1, 14);


-- Tuesday Tier 1
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('01:00', '09:30', 2, 18);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('05:00', '13:30', 2, 3);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('05:15', '09:15', 2, 7);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('07:00', '15:00', 2, 17);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('08:30', '14:30', 2, 4);

-- Tuesday Tier 2
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('06:00', '09:00', 2, 5);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('09:00', '13:00', 2, 16);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('13:00', '17:00', 2, 19);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('15:00', '18:30', 2, 14);


-- Wednesday Tier 1
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('01:00', '09:30', 3, 18);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('05:00', '13:30', 3, 3);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('05:15', '09:15', 3, 7);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('07:00', '15:00', 3, 17);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('08:30', '14:30', 3, 4);

-- Wednesday Tier 2
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('06:00', '09:00', 3, 12);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('09:00', '13:00', 3, 15);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('13:00', '17:00', 3, 8);


-- Thursday Tier 1
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('01:00', '09:30', 4, 18);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('05:00', '13:30', 4, 3);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('05:15', '09:15', 4, 7);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('07:00', '15:00', 4, 17);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('08:30', '14:30', 4, 4);

-- Thursday Tier 2
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('06:00', '09:00', 4, 5);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('09:00', '13:00', 4, 16);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('13:00', '17:00', 4, 19);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('15:00', '18:30', 4, 14);


-- Friday Tier 1
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('01:00', '09:30', 5, 18);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('05:00', '13:30', 5, 3);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('05:15', '09:15', 5, 7);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('07:00', '15:00', 5, 17);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('08:30', '14:30', 5, 4);

-- Friday Tier 2
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('06:00', '09:00', 5, 5);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('09:00', '13:00', 5, 16);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('13:00', '17:00', 5, 19);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('15:00', '18:30', 5, 14);


-- Saturday Tier 1
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('01:00', '09:30', 6, 18);
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('05:15', '12:45', 6, 7);

-- Saturday Tier 2
INSERT INTO roster (starttime, endtime, dow, personid) VALUES ('07:00', '13:00', 6, 7);