
create table person 
(
	id serial primary key,
	extension text not null,
	contacturi text not null,
	name text not null,
	email text,
	support boolean default false,
	accounts boolean default false,
	sales boolean default false,
	tier integer
);

create table roster
(
	id serial primary key,
	starttime time not null, --timezon?
	endtime time not null,
	dow integer, --dayofweek
	personid integer references person(id)
);

create table calllog
(
	id serial primary key,
	sid text not null,
	caller text default 'unknown',
	start time not null,
	finish time,
	called text
);
