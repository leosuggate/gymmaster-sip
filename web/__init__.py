#!/usr/bin/python3
# coding=utf-8
import re
import phonenumbers
import json
import psycopg2
import psycopg2.extras
import holidays

from datetime import date

from flask import Flask, render_template, request, Response, g

from twilio.rest import Client
from twilio.twiml.voice_response import VoiceResponse, Dial, Gather

from slackclient import SlackClient

account_sid = "AC5907b55267809307107172604c17da59"
auth_token = "5bd3dd6cb4ea649353cf713c8a2a6ac2"
caller_id = "+6436683220"
workspace_sid = "WSc62377eb978f29fbe764c18a8e961668"
workflow_sid = "WWe9052ba5263bfc5456da48672a210976"
activity_idle = "WAd1dfa041764397dc3541a730227acf78"
activity_offline = "WAb1bdc171799c8ff7e8cd5ead571a2783"

twilio_client = Client(account_sid, auth_token)

slack_token = "xoxp-16757157815-346124717234-369247341570- \
               114517c13961c05830c50ee35316733d"
slack_client = SlackClient(slack_token)

app = Flask(__name__)


@app.before_request
def before_request():
    import logging
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s - %(name)s - \
                                %(levelname)s - %(message)s')
    app.logger.setLevel(logging.DEBUG)

    # Set up psycopg2 cursor
    try:
        g.conn = psycopg2.connect(database="phone",
                                  user="treshna",
                                  password="prajKaz0",
                                  port="5432",
                                  host="localhost",
                                  sslmode="disable",
                                  cursor_factory=psycopg2.extras.DictCursor)
        g.conn.autocommit = True
        g.db_user = "treshna"
    except psycopg2.Error as e:
        app.logger.error("Failed to connect to DB phone, Host: localhost\n%s"
                         % str(e))


# Cron job this every 15 mins past the hour to update worker activities for
# roster. Twilio will automatically put calls on hold if there is no one
# working, so we can run this function and there is no worry of calls dropping
# during it's run time.
@app.route('/roster')
def roster():
    cursor = g.conn.cursor()

    # Update workers when they're between rostered start and end times.
    # Ignore the afterhours row (id is 25)
    try:
        cursor.execute("""UPDATE person
                        SET isworking = false;
                        UPDATE person
                        SET isworking = true
                        FROM roster
                        WHERE person.id = roster.personid
                        AND dow=(EXTRACT(dow FROM now()))
                        AND CURRENT_TIME BETWEEN
                        roster.starttime AND roster.endtime;
                        SELECT * FROM person
                        WHERE id <> 25;""")
        g.conn.commit()
        workers = cursor.fetchall()
    except psycopg2.DatabaseError as e:
        app.logger.error("Couldn't fetch workers from database.\n%s" % str(e))
        g.conn.rollback()
        cursor.close()

    cursor.close()

    # Update taskrouter activities as is in the database. Whoever is rostered
    # on can have activity set to idle, meaning they will receive calls.
    # Otherwise we update their activity to offline.
    for worker in workers:
        if worker['isworking']:
            twilio_client.taskrouter.workspaces(workspace_sid) \
                                    .workers(worker['sid']) \
                                    .update(activity_sid=activity_idle)
        elif not worker['isworking']:
            twilio_client.taskrouter.workspaces(workspace_sid) \
                                    .workers(worker['sid']) \
                                    .update(activity_sid=activity_offline)
        # In case of error where worker has no rostering assigned
        else:
            # If we can
            if worker['sid'] and worker['name']:
                # Set it to offline just in case, and log error
                twilio_client.taskrouter.workspaces(workspace_sid) \
                                        .workers(worker['sid']) \
                                        .update(activity_sid=activity_offline)
                app.logger.error("Unable to determine worker activity for %s, \
                                setting to offline." % worker['name'])
            # This only happens if there's a dodgy entry in the person table
            else:
                app.logger.error("Unable to determine worker activity. Please \
                                check the database for missing information.")

    return render_template('roster.html', workers=workers)


# A call comes in
@app.route('/incoming_call', methods=['GET', 'POST'])
def incoming_call():
    caller = request.values.get('Caller', 'Unknown Caller')
    sid = request.values.get('CallSid', None)
    welcome = request.args.get('welcome')

    resp = VoiceResponse()

    # Fetch call data using twilio client
    if sid:
        call = twilio_client.calls(sid).fetch()
    else:
        app.logger.error("Fatal: No SID found.")
        raise Exception()

    # This should never happen. Probably due to bad SID if it does.
    if not call:
        app.logger.error("Fatal: unable to find call data with the given SID.")
        raise Exception()

    # Look for afterhours
    cursor = g.conn.cursor()

    try:
        cursor.execute("""INSERT INTO calllog (sid,caller,start)
                              VALUES (%s, %s, %s);
                              SELECT isworking FROM person WHERE id = 25""",
                       (sid, caller, call.start_time))
        data = cursor.fetchone()
        g.conn.commit()
    except psycopg2.DatabaseError as e:
        app.logger.error(e)
        g.conn.rollback
        cursor.close()

    cursor.close()

    if date.today() in holidays.NZ():
        public_holiday = True
    else:
        public_holiday = False

    afterhours = data['isworking']

    if afterhours:
        # Say afterhours message and gather DTMF tones
        gather = Gather(action='/enqueue_call?caller='+caller, method="POST",
                        timeout=2)
        if 'True' in welcome:
            gather.play('static/sounds/afterhours-prefix.gsm')
        gather.play('static/sounds/closed.gsm', loop=3)
        resp.append(gather)

        return str(resp)
    elif public_holiday:
        # Say afterhours message and gather DTMF tones
        gather = Gather(action='/enqueue_call?caller='+caller, method="POST",
                        timeout=2)
        if 'True' in welcome:
            gather.play('static/sounds/public-holiday-prefix.gsm')
        gather.play('static/sounds/closed.gsm', loop=3)
        resp.append(gather)

        return str(resp)

    # Say a welcome message and gather DTMF tones
    gather = Gather(action='/enqueue_call?caller='+caller, method="POST",
                    timeout=2)
    if 'True' in welcome:
        gather.play('static/sounds/welcome.gsm')
    gather.play('static/sounds/menu.gsm')
    resp.append(gather)

    return str(resp)


# Enqueue to Twilio's TaskRouter
@app.route('/enqueue_call', methods=['GET', 'POST'])
def enqueue_call():
    resp = VoiceResponse()
    caller = request.args.get('caller')

    if not caller:
        caller = "Unknown Caller"

    # Gather and comprehend DTMF tones for option and extension handling
    if 'Digits' in request.values:
        digit_pressed = request.values.get('Digits')
        # If customer doesn't dial an extension
        if len(digit_pressed) == 1:
            if digit_pressed == '1':
                message = caller + " called Support"
                with resp.enqueue(None, workflow_sid=workflow_sid) as e:
                    e.task('{"selected_skill": "support"}')
            elif digit_pressed == '2':
                message = caller + " called Sales"
                with resp.enqueue(None, workflow_sid=workflow_sid) as e:
                    e.task('{"selected_skill": "sales"}')
            elif digit_pressed == '3':
                message = caller + " called Accounts"
                with resp.enqueue(None, workflow_sid=workflow_sid) as e:
                    e.task('{"selected_skill": "accounts"}')
            elif digit_pressed == '4':
                message = caller + " called Operator"
                with resp.enqueue(None, workflow_sid=workflow_sid) as e:
                    e.task('{"selected_skill": "operator"}')
            elif digit_pressed == '9':
                resp.redirect('/incoming_call?welcome=False', method='POST')
                return str(resp)

            # If they dialed an option which doesn't exist
            else:
                resp.play('static/sounds/unrecognised.gsm')
                resp.redirect('/incoming_call?welcome=False', method='POST')
                return str(resp)

        # If extension is dialed
        elif len(digit_pressed) == 3:
            cursor = g.conn.cursor()
            cursor.execute("""SELECT extension FROM person""")
            extensions = cursor.fetchall()
            cursor.close()

            found = False
            for row in extensions:
                if digit_pressed == row['extension']:
                    found = True
                    break

            # If the extension can't be found in our list
            if not found:
                resp.play('static/sounds/unrecognised.gsm')
                resp.redirect('/incoming_call?welcome=False', method='POST')
                return str(resp)

            # Otherwise, success!
            cursor = g.conn.cursor()
            cursor.execute("""SELECT contacturi, name FROM person
                              WHERE extension = %s""", (digit_pressed,))
            worker = cursor.fetchone()
            cursor.close()

            # Set slack message
            message = caller + " Called " + worker['name']

            # Dial the specific sip uri for the extension given
            resp = VoiceResponse()
            dial = Dial()
            dial.sip(worker['contacturi'])
            resp.append(dial)
        # When incorrect number of digits are dialed
        # e.g. 2 digits, or >3 digits
        else:
            resp.play('static/sounds/unrecognised.gsm')
            resp.redirect('/incoming_call?welcome=False', method='POST')
            return str(resp)

    else:
        app.logger.error("Fatal: " + caller + " just attempted to call but \
        there was an error adding them to the queue.")
        # Set slack message
        message = "<!here> ERROR: " + caller + " just attempted to call but \
                    there was an error adding them to the queue. Please \
                    follow up with the client as soon as possible."

    # Post the messsage to slack channel 'twilio-calls-test'
    slack_client.api_call(
            "chat.postMessage",
            channel='twilio-calls-test',
            text=message,
            username='Twilio Calls',
            icon_emoji=':robot_face:'
    )

    return str(resp)


@app.route('/enqueue_afterhours', methods=['GET', 'POST'])
def enqueue_afterhours():
    # Should enqueue it to a new TaskQueue "Afterhours", which
    # contains people's cells.

    resp = VoiceResponse()
    caller = request.args.get('caller')

    if not caller:
        caller = 'Unknown Caller'

    # Gather and comprehend DTMF tones for option and extension handling
    if 'Digits' in request.values:
        digit_pressed = request.values.get('Digits')
        # If customer doesn't dial an extension
        if len(digit_pressed) == 1:
            if digit_pressed == '1':  # Voicemail
                # Message for Slack
                message = caller + " called afterhours and left a voicemail"
                # Save voicemail with Twilio
                resp.play('static/sounds/leave-message.gsm')
                resp.record(transcribe_callback='/send_email',
                            timeout=60,
                            play_beep=True)
            elif digit_pressed == '2':  # Urgent support
                # Message for Slack
                message = caller + " called for urgent afterhours support"
                # For now just dial my cell
                resp = VoiceResponse()
                resp.dial('+64221254321')
                # TODO: Dial urgent afterhours support, pay surcharge
                # Something like:
                # with resp.enqueue(None, workflow_sid=workflow_sid) as e:
                #     e.task('{"selected_skill": "afterhours"}')
            elif digit_pressed == '3':  # Everyone
                # Message for Slack
                message = caller + " called afterhours and dialed everyone"
                # Enqueue to everyone

                with resp.enqueue(None, workflow_sid=workflow_sid) as e:
                    e.task()

            # If they dialed an option which doesn't exist
            else:
                resp.play('static/sounds/unrecognised.gsm')
                resp.redirect('/incoming_call?welcome=False', method='POST')

        # If extension is dialed
        elif len(digit_pressed) == 3:
            cursor = g.conn.cursor()
            cursor.execute("""SELECT extension FROM person""")
            extensions = cursor.fetchall()
            cursor.close()

            found = False
            for row in extensions:
                if digit_pressed == row['extension']:
                    found = True
                    break

            # If the extension can't be found in our list
            if not found:
                resp.play('static/sounds/unrecognised.gsm')
                resp.redirect('/incoming_call?welcome=False', method='POST')

            # Otherwise, success!
            cursor = g.conn.cursor()
            cursor.execute("""SELECT contacturi, name FROM person
                              WHERE extension = %s""", (digit_pressed,))
            worker = cursor.fetchone()
            cursor.close()

            # Set slack message
            message = caller + " called " + worker['name']

            # Dial the specific sip uri for the extension given
            resp = VoiceResponse()
            dial = Dial()
            dial.sip(worker['contacturi'])
            resp.append(dial)
            return str(resp)
        # When incorrect number of digits are dialed
        # e.g. 2 digits, or >3 digits
        else:
            resp.play('static/sounds/unrecognised.gsm')
            resp.redirect('/incoming_call?welcome=False', method='POST')

    # I don't think this is possible but who knows
    else:
        app.logger.error("Fatal: {} just attempted to call but there was an error \
                         adding them to the queue.".format(caller))

        # Send error message to Slack
        slack_client.api_call(
            "chat.postMessage",
            channel='twilio-calls-test',
            text='ERROR: {} just attempted to call but there was an \
                   error adding them to the queue. Please follow up with the \
                   client as soon as possible.'.format(caller),
            username='Twilio Calls',
            icon_emoji=':robot_face:'
        )

        raise Exception()

    if not message:
        message = "<!here> Beep boop. Someone called and I errored out! Please \
                    fix me."

    # Post the messsage to slack channel 'twilio-calls-test'
    slack_client.api_call(
            "chat.postMessage",
            channel='twilio-calls-test',
            text=message,
            username='Twilio Calls',
            icon_emoji=':robot_face:'
    )

    return str(resp)


@app.route('/assignment_callback', methods=['GET', 'POST'])
def assignment_callback():
    task_attributes = json.loads(request.values.get('TaskAttributes', None))

    ret = '{"instruction": "dequeue", "from": "' + task_attributes['from'] + \
          '", "post_work_activity_sid": "WAd1dfa041764397dc3541a730227acf78"}'

    return Response(response=ret, status=200, mimetype='application/json')


@app.route('/event_callback', methods=['GET', 'POST'])
def event_callback():
    if 'TaskAttributes' in request.values and \
            'EventType' in request.values:

        task_attributes = json.loads(request.values.get('TaskAttributes'))
        event_type = request.values.get('EventType')

        # If the event is a reservation timeout
        if 'reservation.timeout' in str(event_type) or \
                'task.canceled' in str(event_type):

            sid = task_attributes['worker_call_sid']
            # Terminate call to previous worker. TaskRouter will automatically
            # rescind to the next available worker.
            twilio_client.calls(sid).update(status='canceled')

    return Response(status=204, mimetype='application/json')


@app.route('/send_email', methods=['POST', 'GET'])
def send_email():
    transcription = request.values.get('TranscriptionText')
    caller = request.values.get('Caller')
    message_url = request.values.get('RecordingUrl')

    import smtplib
    from textwrap import dedent

    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText

    # Create new message
    msg = MIMEMultipart()

    # Email Headers
    msg['From'] = "voicemail@gymmasteronline.com"
    msg['To'] = "leo.suggate@gmail.com"  # TODO
    msg['Subject'] = "New message from " + caller

    # Email Body
    body = dedent(
           '''\
           You missed a call from {}.


           The transcription of the message reads:

           {}


           A voicemail is available at {}
           '''
           .format(caller, transcription, message_url)
    )
    msg.attach(MIMEText(body, 'plain'))

    # Connect to SMTP Server
    server = smtplib.SMTP('mail.smtp2go.com', 2525)
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login('twilio-voicemail', 'N2p2ZGp4MG1iaHQw')

    # Set message text to stringified MIME object
    msg_text = msg.as_string()

    # Send the message
    server.sendmail(msg['From'], msg['To'], msg_text)

    return msg_text


# Handle outgoing calls
@app.route('/outgoing_call', methods=['GET', 'POST'])
def outgoing_call():
    # Get number dialled
    to = request.values.get('To', None)
    found_number = re.search("^sip:([+]?[0-9]+)@", to)
    if found_number:
        number = phonenumbers.parse(found_number.group(1))
        to = phonenumbers.format_number(number,
                                        phonenumbers.PhoneNumberFormat.E164)

    resp = VoiceResponse()

    # If outgoing call is to a sip number (unlikely)
    if to.startswith("sip:"):
        app.logger.info(
            '''
            An outgoing call was made, details:

                Type: SIP Call
                To: %s
            '''
            %
            (to)
        )

        with resp.dial(answerOnBridge=True) as d:
            d.sip(to)

    # If outgoing call is to PSTN number
    else:
        region_code = phonenumbers.region_code_for_number(number)

        app.logger.info(
            '''
            An outgoing call was made, details:

                Type: PSTN Call
                To: %s
                Region Code: %s
            '''
            %
            (to, region_code)
        )

        with resp.dial(answerOnBridge=True, callerId=caller_id) as d:

            d.number(to)

    return str(resp)


if __name__ == '__main__':
    app.run()
